import React, { Fragment } from 'react'

function SpotReportRender(props: {id: number, activity: string, latitude: string, longitude: string, date: string}) {


    
    return(
        <Fragment>
            <h5>ACTIVITY: {props.activity}</h5>
            <h5>LATITUDE: {props.latitude}</h5>
            <h5>LONGITUDE: {props.longitude}</h5>
            <h5>DATE: {props.date}</h5>
        </Fragment>
    )
}
export default SpotReportRender;
import './App.css';
import Headquarters from './Headquarters';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <Headquarters />
      </header>
    </div>
  );
}

export default App;

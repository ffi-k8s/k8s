import React, {useState, useEffect} from 'react';
import './Headquarters.css';
import SpotReportRender from './SpotReportRender';
import axios from 'axios';
import config from './config';


class SpotReport extends React.Component<{}, 
    {activity: string, 
    latitude: string, 
    longitude: string, 
    date: string, 
    id: number}> {
    constructor(props: any) {
        super(props);
        this.state = {
            activity: '',
            latitude: '',
            longitude: '',
            date: '',
            id: 0,
        };
      }
}

// 40 dummy SpotReport objects for test purposes
let tempReports: SpotReport[] = []
for (let i: number = 0; i < 40; i++) {
    let report = new SpotReport('');
    report.state = {
        activity: 'Spot' + (i+1),
        latitude: '11.111111',
        longitude: '11.111111',
        date: '11.11.1111',
        id: i+1
    }
    tempReports.push(report)
}

function SpotFormScroll(){
    const [spotReports, setSpotReports] = useState([new SpotReport('')]); 
    const [testReports, setTestReports] = useState(tempReports)
    const [selectedReport, setSelectedReport] = useState(0);


    const url: string = config.api.baseUrl + 'headquarters-api/spot-report'
    
    useEffect(() => {
        // get all spotreports and initialize state with these
        getSpotReports().then((res) => {
            let initReports: any[] = [];
            let count: number = 1;
            res.forEach((element) => {
                let tempReport = new SpotReport("");
                tempReport.state = {
                    activity: element.activity,
                    latitude: element.lat,
                    longitude: element.long,
                    date: element.timeObserved,
                    id: count,
                }
                count++;
                initReports.push(tempReport);
            })
        let newReports = spotReports.concat(initReports)
        setSpotReports(newReports);
        });
    }, [])

    const getSpotReports = async() => {
        let reports: any[] = []
        const response = await axios.get(url).then((response) =>
            response.data.map((element: any) => reports.push(element))
        )
        console.log(response)
        return reports;
    }

    function handleSelect(e: any) {
        let selected: number = +e.target.value;
        setSelectedReport(selected);
    }

    let styles = {
        margin: '5px',
        padding: '1px'
    }

    return(
        <div className="headquarter">
            <h1>Headquarters</h1>
            <div className="container">
                <div className="left">
                    <p>Scrollbar with SPOT-reports</p>
                    <div className="scrollBar">
                        {spotReports.map((element) => (
                            <div className="spotReportContainer" key={"report"+element.state.id} style={styles}>
                                <label>{element.state.activity}</label>
                                <button onClick={handleSelect} value={element.state.id}>Get Details</button>
                            </div>
                        ))}
                    </div>
                </div>
                <div className="right">
                    <div className="details">
                        <p>Detail of SPOT-report</p>
                        {selectedReport === 0 ? 
                        <div></div>
                        :
                        <SpotReportRender id={selectedReport} 
                            activity={spotReports[selectedReport-1].state.activity}
                            latitude={spotReports[selectedReport-1].state.latitude}
                            longitude={spotReports[selectedReport-1].state.longitude}
                            date={spotReports[selectedReport-1].state.date}
                            />
                        }
                    </div>
                    <div className="buttons">
                        <p>Buttons for stuff</p>
                        <div>
                            <p><button type="submit">Request SRS</button></p>
                            <p><button type="submit">Invoke SRS</button></p>
                            <p><button type="submit">View result SRS</button></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default SpotFormScroll;

    .
    ├── Deployments/Test Deployments            # Demo deployment/how to deploy
    ├── Frontend - Dismounted Soldier           # Frontend for Dismounted Soldier
    ├── Frontend-HQ/hq                          # Frontend for HQ (scrubbed)
    ├── scripts                                 # Script for kubernetes setup
    ├── virtual_machine                         # Setup a VM for setup K8s
    ├── k8s                                     # YAML files needed to deploy services
    ├── milestone.pdf                           # Milestone plan for 
    └── README.md                               

![](milestones.png)

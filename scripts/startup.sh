#!/bin/bash

if [ "$EUID" -ne 0 ]
  then echo "Please run as root"
  exit
fi

echo "Installation script for ubuntu with kubernetes and docker" 
sleep 5

apt-get update && sudo apt-get dist-upgrade -y
apt-get install ubuntu-desktop curl docker.io git openssh-server -y
systemctl enable docker && sudo systemctl start docker

#Disabling swap. Because swap dosent work with kubernetes
swapoff -a; sed -i '/swap/d' /etc/fstab

#Addnig kubernets key to repo and installing kubeadm, kubelet and kubectl
curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add
apt-add-repository "deb http://apt.kubernetes.io/ kubernetes-xenial main" && apt-get update && apt-get install kubeadm kubelet kubectl -y

#installing xrdp
apt-get install xrdp -y && adduser xrdp ssl-cert && systemctl restart xrdp

#Need these packages for xrdp to work on  ubuntu 18.04.5
apt-get install xorg-video-abi-23 xserver-xorg-core -y
apt-get install xorgxrdp -y

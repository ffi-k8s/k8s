# Setup script for Ubuntu 18.04

## Running the script
You have to be root to run the script or else you wil get a message "please run as root".
Run this script on the master-node and the workers-nodes in the cluster.

1. Run the script as root on both master and worker-nodes.
- `sudo ./startup.sh`
2. If the script is not executible use the "chmod" command to change it to executible. 
- `chmod +x startup.sh` 
3. This script will update and install the nessesary packages for docker, kubernetes and xrdp.
4. Check that everything is installed correctly

## kubernetes master-node setup
5. After the script is finished you can start with the kubernetes setup. 
6. Select the node you want as master node and run the following command.
- `sudo kubeadm init`
7. This will initialize the master node in the new kubernetes cluster.
8. Once `kubeadm init` is finished, it will display a "kubeadm join" message at the end. Make a note of the whole entry.
9. Next, enter the following to create a directory for the cluster:
- `sudo mkdir -p $HOME/.kube`
- `sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config`
- `sudo chown $(id -u):$(id -g) $HOME/.kube/config`
- Deploy a pod network to the cluster, e.g. Dashboards: `kubectl apply -f https://raw.githubusercontent.com/kubernetes/dashboard/v2.2.0/aio/deploy/recommended.yaml`

## kubernetes worker-node setup
10. Remember the "kubernetes join" message from the master node. You need that command now.
11. Join a worker to the kubernetes cluster with the "kubernetes join" command from the master-node.
- Should look something like this `sudo kubeadm join --discovery-token abcdef.1234567890abcdef --discovery-token-ca-cert-hash sha256:1234..cdef 1.2.3.4:6443`
12. If you have forgotten the discovery token you get a new one with this command (Only run this if you have lost the original discovery token).
- `sudo kubeadm token create --print-join-command`

## Verify the cluster
13. You can see every node in your kubernetes-cluster with this command.
- `sudo kubectl get nodes`

## Any problems with the setup?
14. When running `sudo kubeadm init` it might throw error [ERROR: Swap] and the initialize
sequence will abort. If you get an error that swap is enabled. Disable swap with following command.
- `sudo swapoff -a`
15. Trubble with initializing the master node? You can try to reset kubeadm with this command.
- `sudo kubeadm reset`

16. Following installation of xrdp, the graphical user interface can freeze due to xrdp.
The workaround is to sign in "Ubuntu on Wayland", this option is found clicking the icon
left from "Sign In" button. This will also be the default option by next time login.

### Sources
- https://phoenixnap.com/kb/install-kubernetes-on-ubuntu

# Configuration of virtual machine
If you have a sufficiently powerful computer hardware, it can be an option to simulate the
hardware infrastructure and cluster on your own computer. Using virtual machines (VM) this
allows to pretest cluster solutions without causing failures on the real cluster.

- github
- openssh-server
- flask

Expand the Cluster
Install minimum three VMs as 3 is the minimum required number of nodes in k8s cluster. 
Download Oracle VM VirtualBox from:
`https://www.virtualbox.org/wiki/Downloads`

## Some minimal hardware requirements for Kubernetes Cluster: https://docs.kublr.com/installation/hardware-recommendation/
- In order to provide the good properties of Kubernetes, it is recommended to disable 
swap-memory. As a consequence more main memory (RAM) is needed, or the machine will run very slowly.

1. Make one completely fully configured node (name: ubuntu1)
2. Right-click on it in VirtualBox ane click "Clone..."
3. Select "Expert Mode"
4. Name it "ubuntu3"
5. Clone type: Full Clone
6. Snapshots: Current machine state
7. MAC Address Policy: Generate new MAC addresses for al network adapters
8. Click "Clone"
9. This will then replicate the original node and allows you to expand the cluster with additional nodes, fast and easy.



## 2 Configure Virtual Machine
1. The Oracle VM VirtualBox Manger lists the installed virtual machines.
2. Select the newly installed VM from the list and click "Settings".
3. In "General" > Advanced
4. Shared clipboard = Biredectional
5. Drag'n'Drop = Biredectional
6. System > Motherboard
7. Base memory 2000 MB

### 2.1 System Processor
1. Processor(s) = 2
2. Storage
3. Controller: IDE
4. Attributes: Optical Drive click CD-icon and select choose a disk file.
5. Navigate to the downloaded .iso ubuntu installation image.
6. Audio
7. Turn off Enable Audio Output
8. Turn off Enable Audio

### 2.2 Network 
1. Adapter 1
2. Set to NAT by default
3. Enable Network Adapter (turned ON)
4. Attached to: Bridged Adapter
5. Name: the wireless or wired adapter on your computer
6. Adapter 2,3,4 (click off Enable Network Adapter)
7. RAM minimum 2-3 GB
8. Minimum 2 CPU cores required for kubernetes


# Install Operating System - Ubuntu 18.04.5 LTS 

- After configuring the new virtual machine
	- Start the virtual machine, it is clean and empty installation, but will booth with the Ubuntu installation wizard
	- Click Install Ubuntu
	- Keyboard layout: Norwegian
	- Continue
	- Updates and other software: 
	- Normal installation
	- Donwnload updates while installing Ubuntu
	- Installation type
	- Erase disk and install Ubuntu
	- Click "Install Now"
	- Where are you?: Oslo
	- Who are you
	- Your name: ubuntu2
	- Your computer's name: ubuntu2-VirtualBox
	- Pick a username: ubuntu2
	- Choose a password: xxx
	- Click "Require my password to log in"
	- Next the installation will commense.
	- Installation Complete
	- Requires restarting the virtual machine
	- Press "Restart Now"
	- Please remove installation medium, press ENTER to start restart
	- Next time the virtual machine will boot with Ubuntu
	- It will prompt about new Ubuntu version 20.04.2 LTS Upgrade Available
	- Press "Don't Upgrade" as we stick to LTS18.04.5 LTS and OK to confirm that you declined.
	- Software Updater has been issued for Ubuntu 18.05.4 LTS (73.5 MB). Click "Install Now". 
	- Authentication Required: Provide password to enable further OS update, click Authenticate.
	- Software Updater needs a restart, click the "Restart Now" button.
	- The VM should now run the updated version of Ubuntu 18.04.5 LTS

## Ubuntu 18.04.5 LTS Adaptions
### Install additional tools
	- ifconfig
	- htop
	- git
	
### Clean up dock	
- The dock on left side can be cleaned up
	- Right-click the icons you don't need and select "Remove from Favorites".
	- Help
	- Ubuntu Software
	- LibreOffice Writer
	- Rhythmbox
	- ThunderBird Mail
	- Start up Terminal and right-click icon to "Add to Favorites"
	
### Improve screen resolution
- Click Activities and type "Displays"
	- The Displays option on the Settings program will show as search alternative. Start it.
	-	Resolution is set by default to 800x600 (4:3)
	-	Double it to 1600x1200 (4:3)
	
### Add Guest Additions
-	https://www.virtualbox.org/manual/ch04.html
	-	Needed for shared clipboard between host and guest OS
	-	Menu line > Devices > Insert Guest Additions CD Image...
	-	This will mount a CD drive on desktop: VBox_GAs_6.1.18 and prompted with if you want to install, Would you like to run it?: Run
	-	Type in sudo password and "Authenticate" which will launch the installation in a Terminal session. When finished press Return to close terminal window
	-	Right-click the CD-drive on desktop and click "Eject". 
	-	After rebooting the virtual machine
	
### Install git and run config script
- Open terminal
- Install git command line tool: `sudo apt install git`
- Navigate to the folder where to drop git repository. The repo is private so only valid members
can utilize it.
- Clone K8s git repo: `https://gitlab.com/ffi-k8s/k8s.git`
- Navigate to script: `cd k8s/scripts`
- Run script that installs Kubernetes and Docker: `sudo ./startup.sh`
- Installation script and verification is explained in detail [HERE](https://gitlab.com/ffi-k8s/k8s/-/blob/master/scripts/README.md)
	
### Terminal
-	Open terminal Preferences
	-	Shortcuts
	-	Edit > Copy: Ctrl+C
	-	Edit > Paste: Ctrl+V
	-	Close Preferences windows to confirm
	
### Snapshot
- In the VM overview, of each machine listed there is a dropdown meny on rightmost-side, where you can select view for Details, Snapshots, Logs. 
- Go to Snapshots and click "Take"
- Give the snapshot name: "Initial Installation"
- With this snapshot we can always roll-back the k8s node into a well-known stable working state, in case things go bad.
- If you intend to have unique password for each VM, please do so.

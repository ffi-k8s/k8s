import React, { useState } from 'react';
import DatePicker from 'react-datepicker';
import "react-datepicker/dist/react-datepicker.css";
import axios from 'axios';


function SpotForm() {
    const [form, setForm] = useState({
        activity: "",
        latitude: "",
        longitude: ""
    })
    const [startDate, setStartDate] = useState(new Date());
    const [file, setFile] = useState(new Blob());

    let smallStyle = {
        color: 'red',
        display: 'none',
    };

    const handleFileChange = (e: any) => {
        setFile(e.target.files[0]);
    };

    const handleForm = (e: any) => {
        const { name, value } = e.target;
        setForm((prevState) => ({
            ...prevState,
            [name]: value,
        }));
    }

    function handleDateChange(date: Date) {
        setStartDate(date);
    }

    let file_url = {};

    function uploadSpotReport() {
        // check if file is set before posting request
        if (file.size !== 0) {
            let formData = new FormData();
            formData.append('file', file);

            if (form.activity !== "" && form.latitude !== "" && form.longitude !== "") {
                formData.append('activity', form.activity);
                formData.append('latitude', form.latitude);
                formData.append('longitude', form.longitude);
                formData.append('date', startDate.toString());
                document.getElementById("confirmInputs")?.style.setProperty('display', 'none')
                axios.post("/soldier/upload", formData,
                    {
                        headers: {
                            'accept': 'application/json',
                            'Content-Type': 'multipart/form-data'
                        }
                    })
                    .then((res) => {
                        console.log('SpotReport successfully created');
                        file_url = res.data;
                    }, () => {
                        console.log('SpotReport failed');
                    });
            }
            else {
                document.getElementById("confirmInputs")?.style.setProperty('display', 'initial')
            }
        }
    }

    function notifyHeadquarters() {
        let spotReport = {
            activity: form.activity,
            lat: form.latitude,
            long: form.longitude,
            timeObserved: startDate.toString(),
            fileUrl: file_url
        }
        axios.post("https://core.ist168.work.lab.home.magiclands.de/headquarters-api/headquarters-api/spot-report", spotReport)
            .then((_) => {
                console.log("SpotReport sent to HQ")
            });
        alert("Headquarters notified!")
    }

    return (
        <div className="spotReport">
            <h1>Spot Report</h1>
            <form className="spotReportForm">
                <label className="activity">
                    Activity
                    <input type="text" name="activity" value={form.activity} onChange={handleForm} required />
                </label>
                <label className="latitude">
                    Latitude
                    <input type="number" step="0.000001" name="latitude" value={form.latitude} onChange={handleForm} required />
                </label>
                <label className="longitude">
                    Longitude
                    <input type="number" step="0.000001" name="longitude" value={form.longitude} onChange={handleForm} required />
                </label>
                <label>
                    Time of observation
                    <DatePicker selected={startDate} onChange={handleDateChange} required />
                </label>
                <label>
                    Choose File
                    <input
                        id="file"
                        accept="image/*,video/*"
                        name="upload"
                        onChange={handleFileChange}
                        type="file"
                        required
                    />
                </label>
                <small id="confirmInputs" style={smallStyle}>Please provide input to all fields</small>
            </form>
            <label>
                Upload Spot Report
                <button type="submit" onClick={uploadSpotReport}>
                    Submit
                </button>
            </label>
            <label>
                Notify headquarters
                <button type="submit" id="headquartersButton" onClick={notifyHeadquarters}>Submit</button>
            </label>
        </div>
    )
}

export default SpotForm;

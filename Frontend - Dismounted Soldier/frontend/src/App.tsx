import React from 'react';
import './App.css';
import SpotForm from './SpotForm';

// React deployment
function App() {
  return (
    <div className="App">
      <header className="App-header">
        <SpotForm />
      </header>
    </div>
  );
}

export default App;

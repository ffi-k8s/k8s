const Minio = require('minio');
const config = require('./config')

var minioClient = new Minio.Client(
    {
        endPoint: config.minio.endpoint,
        port: parseInt(config.minio.port),
        useSSL: config.minio.secure,
        accessKey: config.minio.accessKey,
        secretKey: config.minio.secretKey
    }
);

module.exports = minioClient;

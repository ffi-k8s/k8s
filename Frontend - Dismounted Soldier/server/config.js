var config = {};
config.minio = {};

config.minio.endpoint = process.env.MINIO_ENDPOINT;
config.minio.port = process.env.MINIO_PORT;
config.minio.accessKey = process.env.MINIO_ACCESS_KEY;
config.minio.secretKey = process.env.MINIO_SECRET_KEY;
config.minio.secure = process.env.MINIO_SECURE;

module.exports = config;

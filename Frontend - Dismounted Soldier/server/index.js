const express = require('express');
const cors = require("cors");
const multer = require('multer');
const slugify = require('./slugify');
const minioClient = require('./minioClient');

const PORT = process.env.PORT || 3001;

const app = express();
app.use(cors());
app.use(express.json());

app.post("/soldier/upload", multer({storage: multer.memoryStorage()}).single('file'), async (req, res) => {
  let metadata = {
    'activity': req.body.activity,
    'latitude': req.body.latitude,
    'longitude': req.body.longitude,
    'date': req.body.date
  };
  metadata = JSON.stringify(metadata);
  let bucketName = 'uploads';
  let filename = slugify(req.file.originalname.split('.').slice(0, -1).join('.'));
  let fileExtension = req.file.originalname.substring(req.file.originalname.lastIndexOf('.') + 1);
  filename = filename + '.' + fileExtension;
  let bucketExists = await minioClient.bucketExists(bucketName);
  if(!bucketExists) {
      console.log("Did not find bucket, creating it..");
      await minioClient.makeBucket(bucketName, 'Norway');
  }

  await minioClient.putObject(bucketName, filename, req.file.buffer);
  let presignedUrl = await minioClient.presignedGetObject(bucketName, filename, 24 * 60 * 60)
  res.send(presignedUrl)
});

app.listen(PORT, () => {
  console.log(`Server listening on ${PORT}`);
  console.log('–––––––––––––––––––––––––––––––––––––––––––––––––––––-')
});

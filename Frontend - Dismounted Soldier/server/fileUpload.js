const minioClient = require('./minioClient');

function fileUploader(name, jsonName, bucketName, file, jsonfile, metadata){
    minioClient.putObject(bucketName, name, file, metadata, function(error, etag) {
        if(error) {return console.log(error);}
        console.log('File: '+name+' added');
    });
    minioClient.putObject(bucketName, jsonName, jsonfile, function(error, etag) {
      if(error) {return console.log(error);}
      console.log('JSON-file: '+jsonName+' added');
    });
}

function doesBucketExists(name, jsonName, bucketName, file, jsonfile, metadata) {
    minioClient.bucketExists(bucketName, function(error, exists) {
        if (exists) {
            fileUploader(name, jsonName, bucketName, file, jsonfile, metadata);
        }
        if(!exists) {
            console.log("Did not find bucket");
            minioClient.makeBucket(bucketName, 'Norway', function(err) {
                if (err) return console.log('Error creating bucket.', err)
                console.log('Bucket '+bucketName+' created successfully in "Norway".')
                fileUploader(name, jsonName, bucketName, file, jsonfile, metadata);
            });
        }
        if(error) {
          console.log("doesBucketExists: ", error);
        }
    });
}

module.exports = {doesBucketExists};

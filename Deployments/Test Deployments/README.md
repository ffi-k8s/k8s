# How to make a test deployment on Kubernetes

- You can test your kubernetes cluster with the `demo.yaml` file.
- `demo.yaml` is a simple container application that sends ping request to 8.8.8.8

## Deploy a demo 
1. Kubernetes uses `kubectl` to deply and manage pods and applications.
2. To deploy `demo.ymal` with `kubectl`, use the following command.
- `sudo kubectl apply -f demo.yaml`
3. Check deployments with this command:
-  `sudo kubectl get pods`
4. Demo should be running. If demo is not running, then there may be something wrong with the cluster.
5. Check that the demo application is running correctly.
- `sudo kubectl logs demo`
6. The log should display pin replys from 8.8.8.8
7. If everething works correctly, the deplyment can be deleted.
- `sudo kubectl delete -f demo.yaml`
9. Check that deployment is gone.
- `sudo kubectl get pods`
10. If everything worked, then the cluster should be set up correctly.

## Sources
- https://docs.docker.com/get-started/orchestration/

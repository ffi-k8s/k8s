## Start React in docker

You need a working k8s cluster and Docker installed on your computer.

The setup can be different depending on your computer OS and your Kubernetes cluster.

## Test your container before deployment
To create a docker image run `docker build -t soldier/react .` 

Run the docker image with `sudo docker run -d -it  -p 80:80/tcp --name react-app soldier/react`

Open a web-browser and type in `localhost` to confirme that your app is running correct.

See your docker images with this command`docker images -a`

You can now delete your local docker image with `docker rmi container-name`

## Deploying app from  Gitlab container registry
These steps will be a little diffrent depending on what type of Gitlab deployment you are using.

Check the container registry on your Gitlab under `Packages & Registries`. Here you will find the right login information for your Gitlab.

login to Gitlab with docker `docker login registry.gitlab.com`

Build the react app for your container registry with `docker build -t registry.gitlab.com/ffi-k8s/k8s/react .`

Push the Docker image to Gitlab with `docker push registry.gitlab.com/ffi-k8s/k8s/react`

You need to add a secret for the container registry on Github if you want to use a different container registry then docker hub.

Add secret for Gitlab docker-registry `kubectl create secret docker-registry gitlab-auth --docker-server=registry.gitlab.com/ffi-k8s/k8s --docker-username=borgar.foll-ntnu --docker-password=passwd --docker-email=borgarff@stud.ntnu.no`

Pull docker image from Gitlab container registry `kubectl apply -f gitlab-deployment.yaml`

After pulling the image from Gitlab, you can test if your application is running on the Kubernetes Cluster.

To veiw the pods that are running use `kubectl get pods`


## For Docker hub

`docker push docker.io/borgarff/react`

`kubectl apply -f deployment.yaml`
